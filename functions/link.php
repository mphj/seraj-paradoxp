<?php

function bspg($name){
    if (file_exists("pages/page_$name.php")){
        $link = "pages/page_$name.php";
    } else {
        $link = "pages/page_home.php";
    }
    return $link;
}