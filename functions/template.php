<?php

function load_tmp($name, $folderf = '') {
    global $config;
    $tmp = $config['template'];
    if ($config['onwork'] === 0){
        $file = "templates/$tmp/$folderf/tmp_$name.phtml";
        if (file_exists($file)){
            $file = $file;
        } else {
            $file = "templates/tmp_onwork.phtml";
        }
    } elseif ($config['onwork'] === 1) {
        $file = "templates/tmp_onwork.phtml";
    }
    $contentOpen = fopen($file, 'r');
    $content = @fread($contentOpen, filesize($file));
    fclose($contentOpen);
    $content = preg_replace_callback(
        '/@([a-zA-Z0-9_]+)@/',
        function ($matches){
            global $lang;
            $matches[1] = strtolower($matches[1]);
            return (isset($lang[$matches[1]]) ? $lang[$matches[1]] : "");
        },
        $content
    );
    $content = preg_replace_callback(
        '/{{([A-Z0-9_]+)}}/',
        function ($matches){
            global $pagedata;
            $matches[1] = strtolower($matches[1]);
            return (isset($pagedata[$matches[1]]) ? $pagedata[$matches[1]] : "");
        },
        $content
    );
    return $content;
}